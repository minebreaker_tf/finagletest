package sample

import java.util.concurrent.Executors

import com.twitter.finagle.{Http, Service, http}
import com.twitter.logging.Logger
import com.twitter.util.{Await, Future, FuturePool}

object Server extends App {

    val executor = Executors.newFixedThreadPool(20)
    val pool: FuturePool = FuturePool(executor)

    val service = new Service[http.Request, http.Response] {
        override def apply(req: http.Request): Future[http.Response] = {
            pool {

                Logger.get(getClass).info("req m " + Thread.currentThread().getName)

                val res = http.Response(req.version, http.Status.Ok)
                res.contentString = "<h1>hello</h1>"
                res.headerMap.add("content-type", "text/html")

                Thread.sleep(5000)

                res
            }
        }
    }
    val server = Http.serve(":8080", service)
    Await.ready(server)

}
